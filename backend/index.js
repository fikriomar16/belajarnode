import express from "express"
import dotenv from "dotenv"
import cookieParser from "cookie-parser"
import db from "./config/database.js"
import routes from "./routes/index.js"
import cors from "cors"

dotenv.config()

const app = express()
const APP_HOST = process.env.APP_HOST ?? 'http://127.0.0.1'
const APP_PORT = process.env.APP_PORT ?? 5000

try {
	await db.authenticate()
	console.log(`[SUCCESS] Database Connected Successfully.....`)
} catch (error) {
	console.log(error)
}

app.use(cors({ credentials: true, origin: `${APP_HOST}:${APP_PORT}` }))
app.use(cookieParser())
app.use(express.json())
app.use(routes)

app.listen(APP_PORT, () => {
	console.log(`[RUNNING] ${APP_HOST}:${APP_PORT}`)
})