import express from "express"
import { Login, Logout, Register } from "../controllers/AuthController.js"

const router = express.Router()

router.post('/', Login)
router.post('/logout', Logout)
router.post('/register', Register)

export default router