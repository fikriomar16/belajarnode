import express from "express"
import { all, find, create, update, destroy, getUsers } from "../controllers/UserController.js"

const router = express.Router()

router.get('/', getUsers)
router.post('/', create)
router.get('/:id', find)
router.put('/:id', update)
router.delete('/:id', destroy)

export default router