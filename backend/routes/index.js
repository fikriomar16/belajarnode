import express from "express"
import userRoute from './user.js'
import authRoute from './auth.js'
import { refreshToken } from "../controllers/RefreshToken.js"

const router = express.Router()

router.use('/users', userRoute)
router.use('/auth', authRoute)
router.get('/token', refreshToken)

export default router