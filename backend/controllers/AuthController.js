import User from "../models/User.js"
import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"

export const Login = async (req, res) => {
	try {
		const user = await User.findAll({
			where: {
				email: req.body.email
			}
		})
		const match = await bcrypt.compare(req.body.password, user[0].password)
		if (!match) return res.status(400).json({ error: "Wrong Password" })
		const { id, name, email } = user[0]
		const accessToken = await jwt.sign({ id, name, email }, process.env.ACCESS_TOKEN_SECRET, {
			expiresIn: '15s'
		})
		const refreshToken = await jwt.sign({ id, name, email }, process.env.REFRESH_TOKEN_SECRET, {
			expiresIn: '1d'
		})
		await User.update({ remember_token: refreshToken }, {
			where: { id: id }
		})
		res.cookie('refreshToken', refreshToken, {
			httpOnly: true,
			maxAge: 24 * 60 * 60 * 1000
		})
		res.json({ accessToken })
	} catch (error) {
		res.status(404).json({ error: "Something's wrong" })
	}
}

export const Logout = async (req, res) => {
	const { refreshToken } = req.cookies
	if (!refreshToken) return res.sendStatus(204)
	const user = await User.findAll({
		where: {
			remember_token: refreshToken
		}
	})
	if (!user[0]) return res.sendStatus(204)
	const { id } = user[0];
	await User.update({ refresh_token: null }, {
		where: {
			id: id
		}
	});
	res.clearCookie('refreshToken')
	return res.sendStatus(200)
}

export const Register = async (req, res) => {
	const saltRounds = 10
	const { name, username, email, password, location } = req.body
	const encrypted = await bcrypt.hash(password, saltRounds)
	const checkUsername = await User.findOne({ where: { username } })
	const checkEmail = await User.findOne({ where: { email } })
	if (checkUsername != null) {
		res.send({
			status: false,
			message: 'Username already used !!'
		})
	} else if (checkEmail != null) {
		res.send({
			status: false,
			message: 'E-mail already used !!'
		})
	} else {
		const register = await User.create({ name, username, email, location, password: encrypted })
		if (register) {
			res.send({
				status: true,
				message: 'Registration Success !!'
			})
		} else {
			res.send({
				status: false,
				message: 'Registration Failed !!'
			})
		}
	}
}