import User from "../models/User.js"
import { Op } from "sequelize"

export const all = async (req, res) => {
	try {
		const users = await User.findAll()
		res.json(users)
	} catch (error) {
		res.json({
			message: error.message
		})
	}
}

export const getUsers = async (req, res) => {
	const page = parseInt(req.query.page) || 0;
	const limit = parseInt(req.query.limit) || 10;
	const search = req.query.search || "";
	const offset = limit * page;
	const totalRows = await User.count({
		where: {
			[Op.or]: [{
				name: {
					[Op.like]: `%${search}%`
				}
			}, {
				email: {
					[Op.like]: `%${search}%`
				}
			}, {
				username: {
					[Op.like]: `%${search}%`
				}
			}, {
				location: {
					[Op.like]: `%${search}%`
				}
			}]
		}
	});
	const totalPage = Math.ceil(totalRows / limit);
	const result = await User.findAll({
		where: {
			[Op.or]: [{
				name: {
					[Op.like]: `%${search}%`
				}
			}, {
				email: {
					[Op.like]: `%${search}%`
				}
			}, {
				username: {
					[Op.like]: `%${search}%`
				}
			}, {
				location: {
					[Op.like]: `%${search}%`
				}
			}]
		},
		offset: offset,
		limit: limit,
		order: [
			['id', 'ASC']
		]
	});
	res.json({ result, page, limit, totalRows, totalPage });
}

export const find = async (req, res) => {
	try {
		const user = await User.findAll({
			where: {
				id: req.params.id
			}
		})
		if (user && user[0]) {
			res.json(user[0])
		} else {
			res.json({
				message: "Data Not Found"
			})
		}
	} catch (error) {
		res.json({
			'status': false,
			'type': 'error',
			message: error.message
		})
	}
}

export const create = async (req, res) => {
	try {
		const { name, username, email, location } = req.body
		const user = await User.create({ name, username, email, location, password: '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi' })
		await res.json({
			'status': true,
			'type': 'success',
			'message': 'User Added Successfully',
			'user': user.toJSON()
		})
	} catch (error) {
		console.log(error)
	}
}

export const update = async (req, res) => {
	try {
		await User.update(req.body, {
			where: {
				id: req.params.id
			}
		})
		await res.json({
			'status': true,
			'type': 'success',
			'message': 'User Updated Successfully'
		})
	} catch (error) {
		console.log(error)
	}
}

export const destroy = async (req, res) => {
	try {
		await User.destroy({
			where: {
				id: req.params.id
			}
		})
		await res.json({
			'status': true,
			'type': 'success',
			'message': 'User Deleted Successfully'
		})
	} catch (error) {
		console.log(error)
	}
}