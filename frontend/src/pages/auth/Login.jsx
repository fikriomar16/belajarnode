import axios from "axios"
import { lazy, Suspense, useState } from "react"
import { Link, useNavigate } from "react-router-dom"
import toast, { Toaster } from "react-hot-toast"

const FormInput = lazy(() => import('../../components/FormInput'))

const Login = () => {
	let navigate = useNavigate()
	const [login, setLogin] = useState({
		'email': '',
		'password': ''
	})
	const submitHandler = async (e) => {
		e.preventDefault()
		try {
			await axios.post('/auth', login).then(({ data }) => {
				if (data) {
					sessionStorage.setItem('flashmsg', JSON.stringify({ type: 'success', message: 'Successfully Login!' }))
					const { accessToken } = data
					localStorage.setItem('token', accessToken)
					navigate(`/`)
				}
			})
		} catch (error) {
			toast.error(error.response.data.error)
		}
	}
	const changeHandler = e => setLogin({ ...login, [e.target.id]: e.target.value })
	return (
		<>
			<Toaster position="top-right" reverseOrder={ false } />
			<div className="row justify-content-center my-auto">
				<div className="col-md-6">
					<div className="card rounded-4">
						<div className="card-header px-lg-5 px-3 border-0 shadow-sm">Login</div>
						<div className="card-body shadow-sm">
							<form action="" onSubmit={ submitHandler }>
								<Suspense fallback={ <div>Loading...</div> }>
									<FormInput type="email" id="email" placeholder="Insert email here..." value={ login.email } onChange={ changeHandler } label="E-mail:" required={ true } />
									<FormInput type="password" id="password" placeholder="Insert password here..." value={ login.password } onChange={ changeHandler } label="Password:" required={ true } />
									<div className="d-flex justify-content-end">
										<button type="submit" className="btn btn-primary rounded-5"><i className="bi bi-box-arrow-in-right"></i>&nbsp; Login</button>
									</div>
								</Suspense>
							</form>
						</div>
						<div className="card-footer border-0 p-3">
							<div className="row justify-content-between">
								<div className="col">
									<Link to="/" className="btn btn-danger rounded-5 shadow"><i className="bi bi-arrow-left"></i>&nbsp; Back</Link>
								</div>
								<div className="col-auto">
									<span>Doesn't have an account? <Link to="/register" className="text-primary">Register</Link></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}

export default Login