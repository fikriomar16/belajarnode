import axios from "axios"
import { lazy, Suspense, useState } from "react"
import { Link, useNavigate } from "react-router-dom"
import toast, { Toaster } from "react-hot-toast"

const FormInput = lazy(() => import('../../components/FormInput'))

const Register = () => {
	const navigate = useNavigate()
	const [regis, setRegis] = useState({
		name: '',
		username: '',
		email: '',
		password: '',
		location: '',
	})
	const changeHandler = e => setRegis({ ...regis, [e.target.id]: e.target.value })
	const submitHandler = async e => {
		e.preventDefault()
		try {
			await axios.post('/auth/register', regis).then(({ data }) => {
				if (data.status) {
					sessionStorage.setItem('flashmsg', JSON.stringify({ type: 'success', message: data.message }))
					navigate('/login')
				} else {
					toast.error(data.message)
				}
			})
		} catch (error) {
			console.log(error)
		}
	}
	return (
		<>
			<Toaster position="top-right" reverseOrder={ false } />
			<div className="row justify-content-center">
				<div className="col-md-6">
					<div className="card rounded-4">
						<div className="card-header px-lg-5 px-3 border-0 shadow-sm">Register</div>
						<div className="card-body shadow-sm">
							<form action="" method="post" onSubmit={ submitHandler }>
								<Suspense fallback={ <div>Loading...</div> }>
									<FormInput type="text" id="name" placeholder="Insert name here..." value={ regis.name } onChange={ changeHandler } label="Name:" required={ true } />
									<FormInput type="text" id="username" placeholder="Insert username here..." value={ regis.username } onChange={ changeHandler } label="Username:" required={ true } />
									<FormInput type="email" id="email" placeholder="your@example.org" value={ regis.email } onChange={ changeHandler } label="E-mail:" required={ true } />
									<FormInput type="password" id="password" placeholder="Insert password" value={ regis.password } onChange={ changeHandler } label="Password:" required={ true } />
									<FormInput type="text" id="location" placeholder="Insert location here..." value={ regis.location } onChange={ changeHandler } label="Location:" required={ true } />
									<div className="d-flex justify-content-end">
										<button type="submit" className="btn btn-primary rounded-5"><i className="bi bi-box-arrow-in-up"></i>&nbsp; Register</button>
									</div>
								</Suspense>
							</form>
						</div>
						<div className="card-footer border-0 p-3">
							<div className="row justify-content-between">
								<div className="col">
									<Link to="/" className="btn btn-danger rounded-5 shadow"><i className="bi bi-arrow-left"></i>&nbsp; Back</Link>
								</div>
								<div className="col-auto">
									<span>Already have an account? <Link to="/login" className="text-primary">Login</Link></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}

export default Register