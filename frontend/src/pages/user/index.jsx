import { useState, useEffect } from "react"
import axios from "axios"
import { Link } from "react-router-dom"
import ReactPaginate from "react-paginate"
import jwt_decode from 'jwt-decode'

const UserList = () => {
	const [auth, setAuth] = useState(false)
	const [person, setPerson] = useState({})
	const [token, setToken] = useState('')
	const [users, setUsers] = useState([])
	const [page, setPage] = useState(0)
	const [limit, setLimit] = useState(10)
	const [pages, setPages] = useState(0)
	const [rows, setRows] = useState(0)
	const [query, setQuery] = useState("")
	const getUsers = async () => {
		const { data } = await axios.get(`/users?search=${query}&page=${page}&limit=${limit}`);
		setUsers(data.result)
		setPage(data.page)
		setPages(data.totalPage)
		setRows(data.totalRows)
	}
	const deleteAction = async (id) => {
		await axios.delete(`/users/${id}`).then(({ data }) => {
			if (data.status) {
				getUsers()
			} else {
				console.log(data)
			}
		})
	}
	const changePage = ({ selected }) => {
		setPage(selected)
	}
	const getToken = async () => {
		try {
			await axios.get('/token').then(({ data }) => {
				if (data) {
					const { accessToken } = data
					setToken(accessToken)
					const decoded = jwt_decode(accessToken)
					setAuth(true)
					setPerson(decoded)
					localStorage.setItem('token', token)
				}
			})
		} catch (error) {
			setAuth(false)
			setPerson({})
			setToken('')
		}
	}
	useEffect(() => {
		getUsers()
		getToken()
		// eslint-disable-next-line 
	}, [page, query, limit])

	return (
		<>
			<div className="row justify-content-center">
				<div className="col">
					<div className="card rounded-4">
						<div className="card-header border-0 shadow-sm px-lg-5 px-4">
							User Data
						</div>
						<div className="card-body">
							<div className="row justify-content-end">
								<div className="col-auto">
									<Link to="/add" className="btn btn-success rounded-5 shadow-sm">Add User</Link>
								</div>
							</div>
							<div className="row justify-content-center my-2">
								<div className="col-md-5">
									<div className="input-group mb-3">
										<button className="btn btn-dark dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">Limit: { limit }</button>
										<ul className="dropdown-menu">
											<li><button className="dropdown-item" onClick={ () => setLimit(10) }>10</button></li>
											<li><button className="dropdown-item" onClick={ () => setLimit(20) }>20</button></li>
											<li><button className="dropdown-item" onClick={ () => setLimit(50) }>50</button></li>
											<li><button className="dropdown-item" onClick={ () => setLimit(100) }>100</button></li>
										</ul>
										<input type="text" className="form-control" placeholder="Search data..." id="query" onChange={ e => { setPage(0); setQuery(e.target.value) } } />
										<span className="input-group-text" id="label">Search</span>
									</div>

								</div>
							</div>
							<div className="table-responsive">
								<table className="table table-striped table-hover table-borderless caption-top">
									<caption>User Data</caption>
									<thead className="shadow-sm text-uppercase">
										<tr>
											<th>No</th>
											<th>Nama</th>
											<th>Username</th>
											<th>E-Mail</th>
											<th>Location</th>
											{
												auth ?
													<>
														<th className="text-end">Actions</th>
													</>
													:
													<></>
											}
										</tr>
									</thead>
									<tbody>
										{ users.map((user, idx) => (
											<tr key={ user.id }>
												<td>{ (idx + 1) }</td>
												<td>{ user.name }</td>
												<td>{ user.username }</td>
												<td>{ user.email }</td>
												<td>{ user.location }</td>
												<td className="text-end">
													{
														auth ?
															<>
																<div className="dropdown">
																	<button className="btn btn-outline-dark btn-sm border-0 dropdown-toggle rounded-3" type="button" data-bs-toggle="dropdown" aria-expanded="false"></button>
																	<ul className="dropdown-menu border-0 shadow-sm">
																		<li><Link className="dropdown-item" to={ `/users/${user.id}` }>Detail</Link></li>
																		{
																			(person.id === user.id) ?
																				<>
																					<li><Link className="dropdown-item" to={ `/users/${user.id}/edit` }>Edit</Link></li>
																				</>
																				:
																				<>
																					<li><button className="dropdown-item" onClick={ () => deleteAction(user.id) }>Delete</button></li>
																				</>
																		}
																	</ul>
																</div>
															</>
															:
															<></>
													}
												</td>
											</tr>
										)) }
									</tbody>
								</table>
							</div>
							<p className="fw-bold text-center my-2">
								Total Rows: { rows } Page: { rows ? page + 1 : 0 } of { pages }
							</p>
							<div className="d-flex justify-content-center">
								<ReactPaginate
									previousLabel={ '«' }
									nextLabel={ '»' }
									pageCount={ Math.min(limit, pages) }
									onPageChange={ changePage }
									containerClassName={ "pagination rounded-4" }
									pageLinkClassName={ "page-link" }
									previousLinkClassName={ "page-link" }
									nextLinkClassName={ "page-link" }
									activeLinkClassName={ "active" }
									disabledLinkClassName={ "disabled" }
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</ >
	)
}

export default UserList