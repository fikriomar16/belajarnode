import axios from "axios"
import { lazy, Suspense, useEffect, useState } from "react"
import { Link, useNavigate, useParams } from "react-router-dom"

const FormInput = lazy(() => import('../../components/FormInput'))

const EditUser = () => {
	let navigate = useNavigate()
	const { id } = useParams()
	const [user, setUser] = useState({
		name: '',
		username: '',
		email: '',
		location: '',
	})
	const getUser = async () => {
		await axios.get(`/users/${id}`).then(res => {
			const { name, username, email, location } = res.data
			setUser({ name, username, email, location })
		})
	}
	const changeHandler = e => setUser({ ...user, [e.target.id]: e.target.value })
	const submitHandler = async (e) => {
		e.preventDefault()
		await axios.put(`/users/${id}`, user).then(({ data }) => {
			if (data.status) {
				navigate(`/users/${id}`)
			} else {
				console.log(data)
			}
		})
	}
	useEffect(() => {
		getUser()
		// eslint-disable-next-line 
	}, [])
	return (
		<>
			<div className="row justify-content-center">
				<div className="col-md-6">
					<div className="card rounded-4">
						<div className="card-header border-0 shadow-sm px-lg-5 px-3">
							Edit User
						</div>
						<div className="card-body p-3">
							<div className="row mb-3">
								<div className="col">
									<Link to="/users" className="btn btn-primary rounded-5 shadow">Back</Link>
								</div>
							</div>
							<div className="row mb-2">
								<div className="col">
									<form action="" method="post" onSubmit={ submitHandler }>
										<Suspense fallback={ <div>Loading...</div> }>
											<FormInput type="text" id="name" placeholder="Insert name here..." value={ user.name } onChange={ changeHandler } label="Name:" required={ true } />
											<FormInput type="text" id="username" placeholder="Insert username here..." value={ user.username } onChange={ changeHandler } label="Username:" required={ true } />
											<FormInput type="email" id="email" placeholder="your@example.org" value={ user.email } onChange={ changeHandler } label="E-mail:" required={ true } />
											<FormInput type="text" id="location" placeholder="Insert location here..." value={ user.location } onChange={ changeHandler } label="Location:" required={ true } />
										</Suspense>
										<div className="d-flex justify-content-end">
											<button type="submit" className="btn btn-warning rounded-5">Update</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}

export default EditUser