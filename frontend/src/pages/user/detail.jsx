import axios from "axios"
import { useEffect, useState } from "react"
import { Link, useParams } from "react-router-dom"

const UserDetail = (props) => {
	const { id } = useParams()
	const [user, setUser] = useState({})
	const getUser = async () => {
		const { data } = await axios.get(`/users/${id}`)
		setUser(data)
	}
	useEffect(() => {
		getUser()
		// eslint-disable-next-line 
	}, [])
	return (
		<>
			<div className="row justify-content-center">
				<div className="col-md-5">
					<div className="card rounded-4">
						<div className="card-header shadow-sm border-0 px-lg-5 px-3">
							Detail User
						</div>
						<div className="card-body p-lg-4 p-3">
							<div className="row justify-content-between">
								<div className="col-auto">
									<Link to="/users" className="btn btn-primary rounded-5 shadow">Back</Link>
								</div>
								<div className="col-auto">
									<Link to={ `/users/${id}/edit` } className="btn btn-warning rounded-5 shadow">Edit</Link>
								</div>
							</div>
							<div className="row my-3">
								<div className="col">
									<ul className="list-group list-group-flush">
										<li className="list-group-item">Name: <span className="fw-bold">{ user.name }</span></li>
										<li className="list-group-item">Username: <span className="fw-bold">{ user.username }</span></li>
										<li className="list-group-item">E-mail: <span className="fw-bold">{ user.email }</span></li>
										<li className="list-group-item">Location: <span className="fw-bold">{ user.location }</span></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}

export default UserDetail