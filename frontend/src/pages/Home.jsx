const Home = () => {
	return (
		<>
			<div className="row justify-content-center">
				<div className="col-md-6">
					<div className="card rounded-4 shadow border-0">
						<div className="card-body">
							<h2 className="text-center">Welcome to React App</h2>
							<div className="row my-2 px-3">
								<div className="col">
									<ul className="list-group list-group-flush">
										<li className="list-group-item">
											<p className="fw-bold">Tech: </p>
											<ol>
												<li>Node JS</li>
												<li>Express JS</li>
												<li>React JS</li>
											</ol>
										</li>
										<li className="list-group-item">
											<p className="fw-bold">Purpose: </p>
											<ul>
												<li>Learn About JS Stack</li>
												<li>Connecting ReactJS to NodeJS</li>
												<li>Learn About JWT (JSON WEB TOKEN)</li>
											</ul>
										</li>
										<li className="list-group-item">
											<p className="fw-bold">What's on it:</p>
											<ul>
												<li>Table with pagination and search</li>
												<li>Basic CRUD stuff</li>
												<li>Login Attempt with JWT, Register soon</li>
											</ul>
										</li>
										<li className="list-group-item">
											<p className="fw-bold">Soon:</p>
											<ul>
												<li>Redux/Recoil (i feel that though when i'm trying to set auth status on navbar)</li>
												<li>*redirect page when it's unauthenticated (idk what's the name, it doesn't really looks like middleware but still figuring it out)</li>
												<li>More modular code, more reusable components, more efficient code</li>
												<li>Fixing pagination (creating my own module)</li>
											</ul>
										</li>
									</ul>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}

export default Home