import { Link } from 'react-router-dom';

const NotFound = () => {
	return (
		<>
			<div className="row justify-content-center mx-0 my-auto">
				<div className="col-auto">
					<div className="card card-body border-0 shadow rounded-3 p-lg-5 p-3 h-100 text-center">
						<div className="bg-danger pt-3 pb-0 px-4 rounded-3">
							<p className="fw-bold display-1 text-uppercase text-light">OOPS!!</p>
						</div>
						<p className="fw-bold fs-3 text-uppercase text-pink py-5">Page Not Found</p>
						<Link to="/" className="btn btn-lg btn-light text-teal fw-bold text-uppercase rounded-5">Back to main page</Link>
					</div>
				</div>
			</div>
		</>
	);
}

export default NotFound;