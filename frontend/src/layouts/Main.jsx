import Navbar from "../components/Navbar"
import toast, { Toaster } from "react-hot-toast"
import { useEffect } from "react"

const MainLayout = ({ children, navbar = true }) => {
	const flash = JSON.parse(sessionStorage.getItem('flashmsg'))
	useEffect(() => {
		if (flash) {
			if (flash.type === 'success') {
				toast.success(flash.message)
			} else if (flash.type === 'error') {
				toast.error(flash.message)
			} else {
				toast(flash.message,
					{
						icon: '👏',
						style: {
							borderRadius: '10px',
							background: '#333',
							color: '#fff',
						},
					}
				)
			}
		}
		sessionStorage.removeItem('flashmsg')
	}, [flash])

	return (
		<div className="bg-custom min-vh-100 h-100">
			<Toaster position="top-right" reverseOrder={ false } />
			{ navbar ? <Navbar /> : '' }
			<div className="container py-4 my-auto">{ children }</div>
		</div>
	)
}

export default MainLayout