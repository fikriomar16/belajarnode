import { BrowserRouter, Routes, Route } from 'react-router-dom'
import MainLayout from '../layouts/Main'
import Login from '../pages/auth/Login'
import Register from '../pages/auth/Register'
import Home from '../pages/Home'
import NotFound from '../pages/NotFound'
import AddUser from '../pages/user/add'
import UserDetail from '../pages/user/detail'
import EditUser from '../pages/user/edit'
import UserList from '../pages/user/index'

const Router = () => {
	return (
		<BrowserRouter>
			<Routes>
				<Route exact path="/" element={ <MainLayout><Home /></MainLayout> } />
				<Route exact path="/add" element={ <MainLayout><AddUser /></MainLayout> } />
				<Route exact path="/users" element={ <MainLayout><UserList /></MainLayout> } />
				<Route exact path="/users/:id" element={ <MainLayout><UserDetail /></MainLayout> } />
				<Route exact path="/users/:id/edit" element={ <MainLayout><EditUser /></MainLayout> } />
				<Route exact path="/login" element={ <MainLayout navbar={ false }><Login /></MainLayout> } />
				<Route exact path="/register" element={ <MainLayout navbar={ false }><Register /></MainLayout> } />
				<Route path="*" element={ <MainLayout navbar={ false }><NotFound /></MainLayout> } />
			</Routes>
		</BrowserRouter>
	)
}

export default Router