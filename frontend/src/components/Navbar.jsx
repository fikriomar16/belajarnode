import axios from 'axios'
import { useEffect, useState } from 'react'
import { Link, NavLink, useNavigate } from 'react-router-dom'
import jwt_decode from 'jwt-decode'
const Navbar = ({ children }) => {
	let navigate = useNavigate()
	const [auth, setAuth] = useState(false)
	const [user, setUser] = useState({})
	const [expire, setExpire] = useState('')
	const [token, setToken] = useState('')
	const logout = async () => {
		try {
			await axios.post('/auth/logout')
			sessionStorage.setItem('flashmsg', JSON.stringify({ type: 'success', message: 'Successfully Logout!' }))
			localStorage.removeItem('token')
			setAuth(false)
			setUser({})
			setToken('')
			navigate('/login')
		} catch (error) {
			console.log(error)
		}
	}
	const refreshToken = async () => {
		try {
			const response = await axios.get('/token')
			setToken(response.data.accessToken)
			const decoded = jwt_decode(response.data.accessToken)
			setAuth(true)
			setUser(decoded)
			setExpire(decoded.exp)
		} catch (error) {
			setAuth(false)
			setUser({})
			setToken('')
		}
	}
	const axiosJWT = axios.create()
	axiosJWT.interceptors.request.use(async (config) => {
		const currentDate = new Date()
		if (expire * 1000 < currentDate.getTime()) {
			const response = await axios.get('token')
			config.headers.Authorization = `Bearer ${response.data.accessToken}`
			setToken(response.data.accessToken)
			const decoded = jwt_decode(response.data.accessToken)
			setUser(decoded)
			setExpire(decoded.exp)
		}
		return config
	}, (error) => {
		return Promise.reject(error)
	})
	useEffect(() => {
		refreshToken()
	}, [token])
	return (
		<>
			<nav className="navbar navbar-expand-lg bg-light rounded-bottom shadow-sm">
				<div className="container">
					<Link className="navbar-brand fw-bold px-3" to="/">Test-NodeJS</Link>
					<button className="navbar-toggler border-0 shadow-sm small" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						<span className="navbar-toggler-icon small" />
					</button>
					<div className="collapse navbar-collapse" id="navbarNav">
						<ul className="navbar-nav">
							<li className="nav-item">
								<NavLink className="nav-link" activeclassname="active" to="/users">Users</NavLink>
							</li>
							<li className="nav-item">
								<Link className="nav-link" activeclassname="active" to="/404page">404</Link>
							</li>
						</ul>
						<ul className="navbar-nav ms-auto">
							{
								auth
									?
									<>
										<span className="navbar-text me-3">
											{ user.name ?? '' }
										</span>
										<li className="nav-item">
											<NavLink className="nav-link" onClick={ logout } to="#">Logout</NavLink>
										</li>
									</>
									:
									<>
										<li className="nav-item">
											<NavLink className="nav-link" activeclassname="active" to="/login">Login</NavLink>
										</li>
										<li className="nav-item">
											<Link className="nav-link" activeclassname="active" to="/register">Register</Link>
										</li>
									</>
							}
						</ul>
					</div>
				</div>
			</nav>
		</>
	)
}

export default Navbar