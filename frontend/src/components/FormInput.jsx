const FormInput = ({ type, id, placeholder, value, onChange, label, required = false }) => {
	return (
		<>
			<div className="form-floating mb-3">
				<input type={ type } className="form-control rounded-4" id={ id } placeholder={ placeholder } value={ value } onChange={ onChange } required={ required } />
				<label htmlFor="name">{ label }</label>
			</div>
		</>
	)
}

export default FormInput