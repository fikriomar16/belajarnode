# Learning Node JS

> Learning JS Stack using express as backend service and react as frontend

* Backend: `expressjs`
* Frontend: `reactjs`

## _Usage_
1. Backend
* Move directory into `backend/`
* run `yarn install` or `npm install` to install dependencies
* run `yarn start` or `npm start` to run
* backend service will running at port `5000` as default

2. Fronted
* Move directory into `frontend/`
* run `yarn install` or `npm install` to install dependencies
* run `yarn start` or `npm start` to run
* frontend service will running at port `3000` as default